'use strict'

module.exports = {
  devServer: {
    port: 8030,
	// 关闭eslint  空格和tab规则校验
	overlay: {
	            warnings: false,
	            errors: false
	        }
  },
  // 关闭eslint  空格和tab规则校验
  lintOnSave: false
}